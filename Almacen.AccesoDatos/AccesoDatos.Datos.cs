﻿using System.Collections.Generic;
using System.Data;
using Almacen.Entidades;

namespace Almacen.AccesoDatos
{
    public class Datos
    {
        private Conexion _Conn;
        public Datos()
        {
            _Conn = new Conexion("localhost", "root", "", "AlmacenABMODEL", 3306);
        }
        public bool ValidarIdentidad(string user, string password)
        {
            string cadena = string.Format("call pUsuario('{0}','{1}',1)", user, password);
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            int Existe = 0;
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    Existe = int.Parse(row["true"].ToString());
                }
                return (Existe == 1) ? true : false ;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        public List<Herramienta> VerInventario(Herramienta H,int op)
        {
            string cadena = string.Format("call pHerramientas('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {0})",op,H.Codigo,H.Nombre,H.Prioridad,H.Descripcion,H.Estatus,H.Registro);
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            List<Herramienta> Mostrar = new List<Herramienta>();
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    Herramienta He = new Herramienta
                    {
                        Codigo = row["CodigoQR"].ToString(),
                        Nombre = row["NombreHerramienta"].ToString(),
                        Prioridad = row["Prioridad"].ToString(),
                        Descripcion = row["Descripcion"].ToString(),
                        Estatus = row["Estatus"].ToString(),
                        Registro = row["FechaRegistro"].ToString(),
                        Disponibilidad = bool.Parse(row["Disponibilidad"].ToString()),
                    };
                    Mostrar.Add(He);
                }

                return Mostrar;
            }
            catch (System.Exception e)
            {
                Herramienta He = new Herramienta { Codigo = e.Message };
                Mostrar.Add(He);
                return Mostrar;
            }
        }
        public List<PrestamoHerramienta> VerHistoria(PrestamoHerramienta PH, int op)
        {
            string cadena = string.Format("call pPrestamoH('{1}','{2}','{3}','{4}','{5}','{6}','{7}',{0})", op, PH.ID, PH.Persona, PH.Fecha, PH.Hora, PH.Nota, PH.Status,PH.Aula,PH.Materia);
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            List<PrestamoHerramienta> Mostrar = new List<PrestamoHerramienta>();
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    PrestamoHerramienta He = new PrestamoHerramienta
                    {
                        ID = int.Parse(row["ID"].ToString()),
                        Persona = row["FK_Persona"].ToString(),
                        Fecha = row["Fecha"].ToString(),
                        Hora = row["Hora"].ToString(),
                        Nota = row["Nota"].ToString(),
                        Status = bool.Parse(row["Status"].ToString()),
                        Aula = row["FK_Aula"].ToString(),
                        Materia = row["FK_Materia"].ToString(),
                    };
                    Mostrar.Add(He);
                }

                return Mostrar;
            }
            catch (System.Exception e)
            {
                PrestamoHerramienta He = new PrestamoHerramienta { Persona = e.Message };
                Mostrar.Add(He);
                return Mostrar;
            }
        }
        public List<Materia> VerComboBox(string cadena)
        {
            string tabla = string.Format(cadena);
            var DS = _Conn.ObtenerDatos(tabla);
            var DT = DS.Tables[0];
            List<Materia> Mostrar = new List<Materia>();
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    Materia He = new Materia
                    {
                        ID = int.Parse(row["ID"].ToString()),
                        Nombre = row["Nombre"].ToString(),
                    };
                    Mostrar.Add(He);
                }

                return Mostrar;
            }
            catch (System.Exception e)
            {
                Materia He = new Materia { Nombre = e.Message };
                Mostrar.Add(He);
                return Mostrar;
            }
        }
        public void SetEntregado(PrestamoHerramienta PH, int op)
        {
            string cadena = string.Format("call pPrestamoH('{1}','{2}','{3}','{4}','{5}','{6}','{7}',{0})", op, PH.ID, PH.Persona, PH.Fecha, PH.Hora, PH.Nota, PH.Aula, PH.Materia);
            _Conn.Ejecutar(cadena);
        }
        public int VerificarExistencia(string code)
        {
            string cadena = string.Format("call pHerramientas('{0}', '', '', '', '', '', 4)",code);
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            int result = 0;
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    result = int.Parse(row["existe"].ToString());
                }

                return result;
            }
            catch (System.Exception)
            {
                return -1;
            }
        }
        public List<Persona> VerPErsonas(Persona p,int opcion)
        {
            string tabla = string.Format("call pPersonas('{1}', '{2}', '{3}', '{4}', {0})",opcion,p.ID,p.Nombre,p.Paterno,p.Materno);
            var DS = _Conn.ObtenerDatos(tabla);
            var DT = DS.Tables[0];
            List<Persona> Mostrar = new List<Persona>();
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    Persona He = new Persona
                    {
                        ID = row["Control"].ToString(),
                        Nombre = row["Nombres"].ToString(),
                        Paterno = row["Paterno"].ToString(),
                        Materno = row["Materno"].ToString(),
                    };
                    Mostrar.Add(He);
                }

                return Mostrar;
            }
            catch (System.Exception e)
            {
                Persona He = new Persona { Nombre = e.Message };
                Mostrar.Add(He);
                return Mostrar;
            }
        }
        public int AgregarMateria(string Materia)
        {
            string cadena = string.Format("call pMaterias('{0}', 2)", Materia);
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            int result = 0;
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    result = int.Parse(row["existe"].ToString());
                }

                return result;
            }
            catch (System.Exception)
            {
                return -1;
            }
        }
        public int AgregarAula(string aula)
        {
            string cadena = string.Format("call pAulas('{0}', 4)", aula);
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            int result = 0;
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    result = int.Parse(row["existe"].ToString());
                }

                return result;
            }
            catch (System.Exception)
            {
                return -1;
            }
        }
        public int AgregarPersonas(Persona P)
        {
            string cadena = string.Format("call pPersonas('{0}','{1}','{2}','{3}', 1)", P.ID,P.Nombre,P.Paterno,P.Materno);
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            int result = 0;
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    result = int.Parse(row["existe"].ToString());
                }

                return result;
            }
            catch (System.Exception)
            {
                return -1;
            }
        }
        public void PrestarHerramientas(PrestamoHerramientaDetalle PD, int op)
        {
            string cadena = string.Format("call pPrestamoD('{1}','{2}',{0})", op, PD.Prestamo,PD.ID);
            _Conn.Ejecutar(cadena);
        }
        public List<PrestamoHerramientaDetalle> GetDetalle(int prestamo)
        {
            string tabla = string.Format("call pPrestamoD({0},'',2)", prestamo);
            var DS = _Conn.ObtenerDatos(tabla);
            var DT = DS.Tables[0];
            List<PrestamoHerramientaDetalle> Mostrar = new List<PrestamoHerramientaDetalle>();
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    PrestamoHerramientaDetalle He = new PrestamoHerramientaDetalle
                    {
                        ID = row["FK_Herramienta"].ToString(),
                        Prestamo = int.Parse(row["FK_Prestamo"].ToString()),
                        Herramienta = row["NombreHerramienta"].ToString(),
                        Descripcion = row["Descripcion"].ToString(),
                    };
                    Mostrar.Add(He);
                }

                return Mostrar;
            }
            catch (System.Exception e)
            {
                PrestamoHerramientaDetalle He = new PrestamoHerramientaDetalle { Herramienta = e.Message };
                Mostrar.Add(He);
                return Mostrar;
            }
        }
        public List<PrestamoLaboratorio> GetPrestamoLaboratorio(int aula)
        {
            string tabla = string.Format("call pPrestamoA(null,{0},null,null,null,null,3);", aula);
            var DS = _Conn.ObtenerDatos(tabla);
            var DT = DS.Tables[0];
            List<PrestamoLaboratorio> Mostrar = new List<PrestamoLaboratorio>();
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    PrestamoLaboratorio He = new PrestamoLaboratorio
                    {
                        ID = int.Parse(row["ID"].ToString()),
                        Aula = int.Parse(row["FK_Aula"].ToString()),
                        Dia = int.Parse(row["Dia"].ToString()),
                        Entrada = row["HoraEntrada"].ToString(),
                        Salida = row["HoraSalida"].ToString(),
                        Responsable = row["FK_Responsable"].ToString(),
                    };
                    Mostrar.Add(He);
                }

                return Mostrar;
            }
            catch (System.Exception e)
            {
                PrestamoLaboratorio He = new PrestamoLaboratorio { Entrada = e.Message };
                Mostrar.Add(He);
                return Mostrar;
            }
        }
        public bool SetPrestamoLaboratorio(PrestamoLaboratorio Laboratorio, int opcion)
        {
            string cadena = string.Format("call pPrestamoA({1},{2},{3},'{4}','{5}','{6}',{0});", opcion,
                Laboratorio.ID, Laboratorio.Aula, Laboratorio.Dia, Laboratorio.Entrada, Laboratorio.Salida, Laboratorio.Responsable);
            string result = _Conn.Ejecutar(cadena);
            if (result == "true")
                return true;
            else
                return false;
        }

        //sera usado dentro de las configuraciones
        public Configuraciones GetConfiguraciones()
        {
            string tabla = string.Format("call pConfiguracion(true,true,true,2)");
            var DS = _Conn.ObtenerDatos(tabla);
            var DT = DS.Tables[0];
            Configuraciones He = new Configuraciones();
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    He.Confirmacion = bool.Parse(row["Confirmacion"].ToString());
                    He.Verificacion = bool.Parse(row["Verificacion"].ToString());
                    He.Error = bool.Parse(row["Error"].ToString());
                }
                return He;
            }
            catch (System.Exception e)
            {
                He.contraseña = e.Message;
                return He;
            }
        }
        public bool SetConfiguracion(bool confirmacion, bool verificacion,bool errores)
        {
            string cadena = string.Format("call pConfiguracion({0},{1},{2},1)", confirmacion, verificacion,errores);
            string DS = _Conn.Ejecutar(cadena);
            return (DS == "true") ? true : false;
        }
        public bool CambiarPasswordAdmin(string user, string password)
        {
            string cadena = string.Format("call pUsuario('{0}','{1}',2)", user, password);
            string DS = _Conn.Ejecutar(cadena);
            return (DS == "true") ? true : false;
        }
        public string MostrarContrasena()
        {
            string cadena = "call pUsuario('','',3)";
            var DS = _Conn.ObtenerDatos(cadena);
            var DT = DS.Tables[0];
            string result = "";
            try
            {
                foreach (DataRow row in DT.Rows)
                {
                    result = row["contrasenia"].ToString();
                }
                return result;
            }
            catch (System.Exception e)
            {
                result = e.Message;
                return result;
            }
        }
        public bool AgregarHerramientas(Herramienta herramienta)
        {
            string cadena = string.Format("call pHerramientas('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', 2)", herramienta.Codigo, herramienta.Nombre, herramienta.Prioridad,herramienta.Descripcion,herramienta.Estatus,herramienta.Registro);
            string DS = _Conn.Ejecutar(cadena);
            return (DS == "true") ? true : false;
        }
    }
}
