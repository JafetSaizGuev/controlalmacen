﻿namespace Almacen.Entidades
{
    public class Materia
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
    }
}
