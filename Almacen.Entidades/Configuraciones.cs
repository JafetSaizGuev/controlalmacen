﻿namespace Almacen.Entidades
{
    public class Configuraciones
    {
        public string contraseña { get; set; }
        public bool Confirmacion { get; set; }
        public bool Verificacion { get; set; }
        public bool Error { get; set; }
    }
}
