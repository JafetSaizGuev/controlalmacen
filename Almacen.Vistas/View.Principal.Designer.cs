﻿namespace Almacen.Vistas
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.pnlBarraTop = new System.Windows.Forms.Panel();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblAppTitle = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.pnlLateral = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnInventario = new System.Windows.Forms.Button();
            this.pnlPendientes = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnCurso = new System.Windows.Forms.Button();
            this.btnHistorial = new System.Windows.Forms.Button();
            this.btnPendientes = new System.Windows.Forms.Button();
            this.pnlPrestamo = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnHerramienta = new System.Windows.Forms.Button();
            this.btnLaboratorio = new System.Windows.Forms.Button();
            this.btnPrestamo = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnConfiguracion = new System.Windows.Forms.Button();
            this.imgLogo = new System.Windows.Forms.PictureBox();
            this.pnlCuerpo = new System.Windows.Forms.Panel();
            this.pnlNotificaciones = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlBarraTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.pnlLateral.SuspendLayout();
            this.panel7.SuspendLayout();
            this.pnlPendientes.SuspendLayout();
            this.pnlPrestamo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).BeginInit();
            this.pnlNotificaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBarraTop
            // 
            this.pnlBarraTop.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlBarraTop.Controls.Add(this.btnMinimizar);
            this.pnlBarraTop.Controls.Add(this.pictureBox2);
            this.pnlBarraTop.Controls.Add(this.lblAppTitle);
            this.pnlBarraTop.Controls.Add(this.btnCerrar);
            this.pnlBarraTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBarraTop.Location = new System.Drawing.Point(0, 0);
            this.pnlBarraTop.Name = "pnlBarraTop";
            this.pnlBarraTop.Size = new System.Drawing.Size(900, 35);
            this.pnlBarraTop.TabIndex = 7;
            this.pnlBarraTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlBarraTop_MouseDown);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Image")));
            this.btnMinimizar.Location = new System.Drawing.Point(841, 3);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(25, 25);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 3;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(26, 26);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlBarraTop_MouseDown);
            // 
            // lblAppTitle
            // 
            this.lblAppTitle.AutoSize = true;
            this.lblAppTitle.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppTitle.Location = new System.Drawing.Point(62, 6);
            this.lblAppTitle.Name = "lblAppTitle";
            this.lblAppTitle.Size = new System.Drawing.Size(92, 26);
            this.lblAppTitle.TabIndex = 1;
            this.lblAppTitle.Text = "Almacen";
            this.lblAppTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlBarraTop_MouseDown);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(872, 4);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(25, 25);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 0;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // pnlLateral
            // 
            this.pnlLateral.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlLateral.Controls.Add(this.panel7);
            this.pnlLateral.Controls.Add(this.panel3);
            this.pnlLateral.Controls.Add(this.panel1);
            this.pnlLateral.Controls.Add(this.panel2);
            this.pnlLateral.Controls.Add(this.panel4);
            this.pnlLateral.Controls.Add(this.btnConfiguracion);
            this.pnlLateral.Controls.Add(this.imgLogo);
            this.pnlLateral.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLateral.Location = new System.Drawing.Point(0, 35);
            this.pnlLateral.Name = "pnlLateral";
            this.pnlLateral.Size = new System.Drawing.Size(200, 565);
            this.pnlLateral.TabIndex = 8;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnInventario);
            this.panel7.Controls.Add(this.pnlPendientes);
            this.panel7.Controls.Add(this.btnPendientes);
            this.panel7.Controls.Add(this.pnlPrestamo);
            this.panel7.Controls.Add(this.btnPrestamo);
            this.panel7.Location = new System.Drawing.Point(10, 149);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(188, 370);
            this.panel7.TabIndex = 9;
            // 
            // btnInventario
            // 
            this.btnInventario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInventario.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInventario.FlatAppearance.BorderSize = 0;
            this.btnInventario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnInventario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInventario.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInventario.Image = ((System.Drawing.Image)(resources.GetObject("btnInventario.Image")));
            this.btnInventario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInventario.Location = new System.Drawing.Point(0, 284);
            this.btnInventario.Name = "btnInventario";
            this.btnInventario.Size = new System.Drawing.Size(188, 47);
            this.btnInventario.TabIndex = 3;
            this.btnInventario.Text = "Inventario";
            this.btnInventario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInventario.UseVisualStyleBackColor = true;
            this.btnInventario.Click += new System.EventHandler(this.btnInventario_Click);
            // 
            // pnlPendientes
            // 
            this.pnlPendientes.Controls.Add(this.panel9);
            this.pnlPendientes.Controls.Add(this.panel10);
            this.pnlPendientes.Controls.Add(this.btnCurso);
            this.pnlPendientes.Controls.Add(this.btnHistorial);
            this.pnlPendientes.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPendientes.Location = new System.Drawing.Point(0, 189);
            this.pnlPendientes.Name = "pnlPendientes";
            this.pnlPendientes.Size = new System.Drawing.Size(188, 95);
            this.pnlPendientes.TabIndex = 14;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Purple;
            this.panel9.Location = new System.Drawing.Point(1, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(10, 47);
            this.panel9.TabIndex = 10;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Purple;
            this.panel10.Location = new System.Drawing.Point(1, 47);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(10, 47);
            this.panel10.TabIndex = 12;
            // 
            // btnCurso
            // 
            this.btnCurso.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCurso.FlatAppearance.BorderSize = 0;
            this.btnCurso.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnCurso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCurso.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurso.Image = ((System.Drawing.Image)(resources.GetObject("btnCurso.Image")));
            this.btnCurso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCurso.Location = new System.Drawing.Point(11, 0);
            this.btnCurso.Name = "btnCurso";
            this.btnCurso.Size = new System.Drawing.Size(178, 47);
            this.btnCurso.TabIndex = 9;
            this.btnCurso.Text = "En Curso";
            this.btnCurso.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCurso.UseVisualStyleBackColor = true;
            this.btnCurso.Click += new System.EventHandler(this.btnCurso_Click);
            // 
            // btnHistorial
            // 
            this.btnHistorial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHistorial.FlatAppearance.BorderSize = 0;
            this.btnHistorial.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnHistorial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHistorial.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistorial.Image = ((System.Drawing.Image)(resources.GetObject("btnHistorial.Image")));
            this.btnHistorial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHistorial.Location = new System.Drawing.Point(11, 47);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(178, 47);
            this.btnHistorial.TabIndex = 11;
            this.btnHistorial.Text = "Historial";
            this.btnHistorial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHistorial.UseVisualStyleBackColor = true;
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // btnPendientes
            // 
            this.btnPendientes.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnPendientes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPendientes.FlatAppearance.BorderSize = 0;
            this.btnPendientes.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnPendientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPendientes.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPendientes.Image = ((System.Drawing.Image)(resources.GetObject("btnPendientes.Image")));
            this.btnPendientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPendientes.Location = new System.Drawing.Point(0, 142);
            this.btnPendientes.Name = "btnPendientes";
            this.btnPendientes.Size = new System.Drawing.Size(188, 47);
            this.btnPendientes.TabIndex = 2;
            this.btnPendientes.Text = "Pendientes";
            this.btnPendientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPendientes.UseVisualStyleBackColor = true;
            // 
            // pnlPrestamo
            // 
            this.pnlPrestamo.Controls.Add(this.panel5);
            this.pnlPrestamo.Controls.Add(this.panel6);
            this.pnlPrestamo.Controls.Add(this.btnHerramienta);
            this.pnlPrestamo.Controls.Add(this.btnLaboratorio);
            this.pnlPrestamo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPrestamo.Location = new System.Drawing.Point(0, 47);
            this.pnlPrestamo.Name = "pnlPrestamo";
            this.pnlPrestamo.Size = new System.Drawing.Size(188, 95);
            this.pnlPrestamo.TabIndex = 13;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Purple;
            this.panel5.Location = new System.Drawing.Point(1, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 47);
            this.panel5.TabIndex = 10;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Purple;
            this.panel6.Location = new System.Drawing.Point(1, 47);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 47);
            this.panel6.TabIndex = 12;
            // 
            // btnHerramienta
            // 
            this.btnHerramienta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHerramienta.FlatAppearance.BorderSize = 0;
            this.btnHerramienta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnHerramienta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHerramienta.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHerramienta.Image = ((System.Drawing.Image)(resources.GetObject("btnHerramienta.Image")));
            this.btnHerramienta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHerramienta.Location = new System.Drawing.Point(11, 0);
            this.btnHerramienta.Name = "btnHerramienta";
            this.btnHerramienta.Size = new System.Drawing.Size(178, 47);
            this.btnHerramienta.TabIndex = 9;
            this.btnHerramienta.Text = "Herramientas";
            this.btnHerramienta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHerramienta.UseVisualStyleBackColor = true;
            this.btnHerramienta.Click += new System.EventHandler(this.btnHerramienta_Click);
            // 
            // btnLaboratorio
            // 
            this.btnLaboratorio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLaboratorio.FlatAppearance.BorderSize = 0;
            this.btnLaboratorio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnLaboratorio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLaboratorio.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLaboratorio.Image = ((System.Drawing.Image)(resources.GetObject("btnLaboratorio.Image")));
            this.btnLaboratorio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLaboratorio.Location = new System.Drawing.Point(11, 47);
            this.btnLaboratorio.Name = "btnLaboratorio";
            this.btnLaboratorio.Size = new System.Drawing.Size(178, 47);
            this.btnLaboratorio.TabIndex = 11;
            this.btnLaboratorio.Text = "Laboratorios";
            this.btnLaboratorio.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLaboratorio.UseVisualStyleBackColor = true;
            this.btnLaboratorio.Click += new System.EventHandler(this.btnLaboratorio_Click);
            // 
            // btnPrestamo
            // 
            this.btnPrestamo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnPrestamo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPrestamo.FlatAppearance.BorderSize = 0;
            this.btnPrestamo.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnPrestamo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrestamo.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrestamo.Image = ((System.Drawing.Image)(resources.GetObject("btnPrestamo.Image")));
            this.btnPrestamo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrestamo.Location = new System.Drawing.Point(0, 0);
            this.btnPrestamo.Name = "btnPrestamo";
            this.btnPrestamo.Size = new System.Drawing.Size(188, 47);
            this.btnPrestamo.TabIndex = 1;
            this.btnPrestamo.Text = "Prestamo";
            this.btnPrestamo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrestamo.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Purple;
            this.panel3.Location = new System.Drawing.Point(0, 431);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 49);
            this.panel3.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Purple;
            this.panel1.Location = new System.Drawing.Point(0, 149);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(10, 141);
            this.panel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Purple;
            this.panel2.Location = new System.Drawing.Point(0, 290);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 141);
            this.panel2.TabIndex = 6;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Purple;
            this.panel4.Location = new System.Drawing.Point(0, 519);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 46);
            this.panel4.TabIndex = 8;
            // 
            // btnConfiguracion
            // 
            this.btnConfiguracion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfiguracion.FlatAppearance.BorderSize = 0;
            this.btnConfiguracion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnConfiguracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfiguracion.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfiguracion.Image = ((System.Drawing.Image)(resources.GetObject("btnConfiguracion.Image")));
            this.btnConfiguracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfiguracion.Location = new System.Drawing.Point(10, 518);
            this.btnConfiguracion.Name = "btnConfiguracion";
            this.btnConfiguracion.Size = new System.Drawing.Size(188, 47);
            this.btnConfiguracion.TabIndex = 4;
            this.btnConfiguracion.Text = "Configuracion";
            this.btnConfiguracion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfiguracion.UseVisualStyleBackColor = true;
            this.btnConfiguracion.Click += new System.EventHandler(this.btnConfiguracion_Click);
            // 
            // imgLogo
            // 
            this.imgLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgLogo.Image = ((System.Drawing.Image)(resources.GetObject("imgLogo.Image")));
            this.imgLogo.Location = new System.Drawing.Point(23, 19);
            this.imgLogo.Name = "imgLogo";
            this.imgLogo.Size = new System.Drawing.Size(155, 111);
            this.imgLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgLogo.TabIndex = 0;
            this.imgLogo.TabStop = false;
            this.imgLogo.Click += new System.EventHandler(this.imgLogo_Click);
            // 
            // pnlCuerpo
            // 
            this.pnlCuerpo.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlCuerpo.Location = new System.Drawing.Point(200, 85);
            this.pnlCuerpo.Name = "pnlCuerpo";
            this.pnlCuerpo.Size = new System.Drawing.Size(700, 515);
            this.pnlCuerpo.TabIndex = 9;
            // 
            // pnlNotificaciones
            // 
            this.pnlNotificaciones.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlNotificaciones.Controls.Add(this.pictureBox1);
            this.pnlNotificaciones.Location = new System.Drawing.Point(200, 35);
            this.pnlNotificaciones.Name = "pnlNotificaciones";
            this.pnlNotificaciones.Size = new System.Drawing.Size(700, 52);
            this.pnlNotificaciones.TabIndex = 11;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(654, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(43, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.pnlNotificaciones);
            this.Controls.Add(this.pnlCuerpo);
            this.Controls.Add(this.pnlLateral);
            this.Controls.Add(this.pnlBarraTop);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Almacen";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.pnlBarraTop.ResumeLayout(false);
            this.pnlBarraTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.pnlLateral.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.pnlPendientes.ResumeLayout(false);
            this.pnlPrestamo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).EndInit();
            this.pnlNotificaciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBarraTop;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.Panel pnlLateral;
        private System.Windows.Forms.Panel pnlCuerpo;
        private System.Windows.Forms.Button btnConfiguracion;
        private System.Windows.Forms.Button btnInventario;
        private System.Windows.Forms.Button btnPendientes;
        private System.Windows.Forms.Button btnPrestamo;
        private System.Windows.Forms.PictureBox imgLogo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblAppTitle;
        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.Panel pnlNotificaciones;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlPrestamo;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnHerramienta;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnLaboratorio;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel pnlPendientes;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnCurso;
        private System.Windows.Forms.Button btnHistorial;
    }
}

