﻿namespace Almacen.Vistas
{
    partial class frmConfiguracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfiguracion));
            this.label1 = new System.Windows.Forms.Label();
            this.txtContraseña = new System.Windows.Forms.TextBox();
            this.btnMostrar = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.chbVerificacion = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chbConfirmacion = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chbError = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnAgregarInventario = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAceptarCambios = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btnMostrar)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Contraseña de Administrador";
            // 
            // txtContraseña
            // 
            this.txtContraseña.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtContraseña.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContraseña.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContraseña.Location = new System.Drawing.Point(369, 24);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.PasswordChar = '*';
            this.txtContraseña.Size = new System.Drawing.Size(204, 26);
            this.txtContraseña.TabIndex = 4;
            // 
            // btnMostrar
            // 
            this.btnMostrar.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnMostrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMostrar.Image = ((System.Drawing.Image)(resources.GetObject("btnMostrar.Image")));
            this.btnMostrar.Location = new System.Drawing.Point(573, 24);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(26, 26);
            this.btnMostrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMostrar.TabIndex = 6;
            this.btnMostrar.TabStop = false;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnMostrar);
            this.panel1.Controls.Add(this.txtContraseña);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(700, 75);
            this.panel1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(418, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "¿Pedir verificación al entrar a pestañas de control?";
            // 
            // panel4
            // 
            this.panel4.CausesValidation = false;
            this.panel4.Controls.Add(this.chbVerificacion);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 75);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(700, 75);
            this.panel4.TabIndex = 10;
            // 
            // chbVerificacion
            // 
            this.chbVerificacion.AutoSize = true;
            this.chbVerificacion.Location = new System.Drawing.Point(510, 32);
            this.chbVerificacion.Name = "chbVerificacion";
            this.chbVerificacion.Size = new System.Drawing.Size(15, 14);
            this.chbVerificacion.TabIndex = 8;
            this.chbVerificacion.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.CausesValidation = false;
            this.panel3.Controls.Add(this.chbConfirmacion);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 150);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(700, 75);
            this.panel3.TabIndex = 11;
            // 
            // chbConfirmacion
            // 
            this.chbConfirmacion.AutoSize = true;
            this.chbConfirmacion.Location = new System.Drawing.Point(510, 32);
            this.chbConfirmacion.Name = "chbConfirmacion";
            this.chbConfirmacion.Size = new System.Drawing.Size(15, 14);
            this.chbConfirmacion.TabIndex = 8;
            this.chbConfirmacion.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(349, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "¿Mostrar confirmación al  realizar acción?";
            // 
            // panel2
            // 
            this.panel2.CausesValidation = false;
            this.panel2.Controls.Add(this.chbError);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 225);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(700, 75);
            this.panel2.TabIndex = 12;
            // 
            // chbError
            // 
            this.chbError.AutoSize = true;
            this.chbError.Location = new System.Drawing.Point(510, 30);
            this.chbError.Name = "chbError";
            this.chbError.Size = new System.Drawing.Size(15, 14);
            this.chbError.TabIndex = 10;
            this.chbError.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(240, 23);
            this.label4.TabIndex = 9;
            this.label4.Text = "¿Mostrar mensajes de error?";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Purple;
            this.panel6.Location = new System.Drawing.Point(29, 14);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 47);
            this.panel6.TabIndex = 16;
            // 
            // btnAgregarInventario
            // 
            this.btnAgregarInventario.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnAgregarInventario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarInventario.FlatAppearance.BorderSize = 0;
            this.btnAgregarInventario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnAgregarInventario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarInventario.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarInventario.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarInventario.Image")));
            this.btnAgregarInventario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarInventario.Location = new System.Drawing.Point(39, 14);
            this.btnAgregarInventario.Name = "btnAgregarInventario";
            this.btnAgregarInventario.Size = new System.Drawing.Size(178, 47);
            this.btnAgregarInventario.TabIndex = 15;
            this.btnAgregarInventario.Text = "Agregar al Inventario";
            this.btnAgregarInventario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgregarInventario.UseCompatibleTextRendering = true;
            this.btnAgregarInventario.UseVisualStyleBackColor = false;
            this.btnAgregarInventario.Click += new System.EventHandler(this.btnAgregarInventario_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Purple;
            this.panel5.Location = new System.Drawing.Point(500, 448);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 47);
            this.panel5.TabIndex = 14;
            // 
            // btnAceptarCambios
            // 
            this.btnAceptarCambios.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnAceptarCambios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAceptarCambios.FlatAppearance.BorderSize = 0;
            this.btnAceptarCambios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Purple;
            this.btnAceptarCambios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceptarCambios.Font = new System.Drawing.Font("Constantia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptarCambios.Image = ((System.Drawing.Image)(resources.GetObject("btnAceptarCambios.Image")));
            this.btnAceptarCambios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAceptarCambios.Location = new System.Drawing.Point(510, 448);
            this.btnAceptarCambios.Name = "btnAceptarCambios";
            this.btnAceptarCambios.Size = new System.Drawing.Size(178, 47);
            this.btnAceptarCambios.TabIndex = 13;
            this.btnAceptarCambios.Text = "Realizar";
            this.btnAceptarCambios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAceptarCambios.UseCompatibleTextRendering = true;
            this.btnAceptarCambios.UseVisualStyleBackColor = false;
            this.btnAceptarCambios.Click += new System.EventHandler(this.btnAceptarCambios_Click);
            // 
            // panel7
            // 
            this.panel7.CausesValidation = false;
            this.panel7.Controls.Add(this.panel6);
            this.panel7.Controls.Add(this.btnAgregarInventario);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 300);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(700, 75);
            this.panel7.TabIndex = 15;
            // 
            // frmConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(700, 515);
            this.ControlBox = false;
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.btnAceptarCambios);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 515);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(700, 515);
            this.Name = "frmConfiguracion";
            this.ShowIcon = false;
            this.Text = "View";
            this.Load += new System.EventHandler(this.frmConfiguracion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnMostrar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.PictureBox btnMostrar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox chbVerificacion;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chbConfirmacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnAceptarCambios;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnAgregarInventario;
        private System.Windows.Forms.CheckBox chbError;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel7;
    }
}