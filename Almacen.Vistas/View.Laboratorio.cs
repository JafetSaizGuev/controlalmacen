﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Almacen.LogicaNegocios;
using Almacen.Entidades;
using System.IO;
using System.Xml.Serialization;

namespace Almacen.Vistas
{
    public partial class frmLaboratorio : Form
    {
        private cAlmacen ControlAlmacen;
        private List<Materia> Laboratorios;
        private Configuraciones MyConfig;
        private frmNuevoLab NL;
        public frmLaboratorio(Configuraciones Config)
        {
            InitializeComponent();
            ControlAlmacen = new cAlmacen();
            this.MyConfig = Config;
        }

        private void frmLaboratorio_Load(object sender, EventArgs e)
        {
            dgvHorarios.DataSource = ControlAlmacen.GetHorarios();
            Laboratorios = ControlAlmacen.Combos("call pAulas(null, 2)");
            for (int i = 0; i < Laboratorios.Count; i++)
            {
                cmbLaboratorios.Items.Add(Laboratorios[i].Nombre);
            }
        }

        private void cmbLaboratorios_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostrar();
        }
        private void cmbLaboratorios_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MyConfig.Verificacion)
                {
                    if (Mensaje("Verificar", "¿Esta seguro de agregar el nuevo lavoratorio?") == DialogResult.OK)
                        GuardarLaboratorio();
                }
                else
                    GuardarLaboratorio();
            }
        }
        private DialogResult Mensaje(string titulo,string contenido)
        {
            frmMensaje M = new frmMensaje(titulo, contenido);
            DialogResult R = M.ShowDialog();
            return R;
        }
        private void GuardarLaboratorio()
        {
            bool accion = ControlAlmacen.AgregarAula(cmbLaboratorios.Text);
            if (accion)
            {
                if (MyConfig.Confirmacion)
                    Mensaje("Correcto", "El nuevo laboratorio se agrego con exito");
            }
            else
            {
                if (MyConfig.Error)
                    Mensaje("Error", "No se pudo agregar el nuevo laboratorio");
            }
        }

        private void dgvHorarios_MouseUp(object sender, MouseEventArgs e)
        {
            var some = dgvHorarios.SelectedCells;
            int lab = 0;
            for (int i = 0; i < Laboratorios.Count; i++)
            {
                if (Laboratorios[i].Nombre == cmbLaboratorios.Text)
                    lab = Laboratorios[i].ID;
            }
            int accion = 0;
            List<MandarNuevo> Mandar = new List<MandarNuevo>();
            for (int i = 0; i < some.Count; i++)
            {
                if (some[i].Value != null)
                    accion++;
                Mandar.Add(new MandarNuevo
                {
                    Laboratorio = lab,
                    Dia = dgvHorarios.SelectedCells[i].ColumnIndex,
                    Hora = dgvHorarios.SelectedCells[i].RowIndex,
                });
            }
            if (accion == 0)
                AbrirNuevaVentana(Mandar);
            else if (accion == some.Count)
                EliminarTodo(Mandar, lab);
            else
                AccionMixta(some, lab);
            Mostrar();
        }
        private void AbrirNuevaVentana(List<MandarNuevo> Prestamos)
        {
            if (!string.IsNullOrEmpty(cmbLaboratorios.Text))
            {
                if (NL == null)
                {
                    NL = new frmNuevoLab(Prestamos, MyConfig);
                    NL.Show();
                }
                else if (NL.IsDisposed)
                {
                    NL = new frmNuevoLab(Prestamos, MyConfig);
                    NL.Show();
                }
                else
                    NL.Focus();
            }
        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            Mostrar();
        }
        private void Mostrar()
        {
            Laboratorios = null;
            Laboratorios = ControlAlmacen.Combos("call pAulas('', 2)");
            int eleccion = 0;
            for (int i = 0; i < Laboratorios.Count; i++)
            {
                if (cmbLaboratorios.Text == Laboratorios[i].Nombre)
                    eleccion = Laboratorios[i].ID;
            }
            List<PrestamoLaboratorio> PL = ControlAlmacen.GetPrestamoLaboratorio(eleccion);
            dgvHorarios.DataSource = null;
            dgvHorarios.DataSource = ControlAlmacen.GetHorarios(PL);
        }
        private void EliminarTodo(List<MandarNuevo> eliminar,int lab)
        {
            if (MyConfig.Verificacion)
            {
                frmMensaje M = new frmMensaje("Verificar", "¿Desea dejar libre esta hora para el laboratorio?");
                DialogResult R = M.ShowDialog();
                if(R == DialogResult.OK)
                {
                    Eliminar(eliminar, lab);
                }
            }
            else
                Eliminar(eliminar, lab);
        }
        private void AccionMixta(DataGridViewSelectedCellCollection coleccion,int lab)
        {
            DialogResult R = new DialogResult();
            if (MyConfig.Verificacion)
            {
                frmMensaje M = new frmMensaje("Continuar", "¿Desea dejar libre los espacios ocupados antes de continuar?");
                R = M.ShowDialog();
            }
            if(R == DialogResult.OK || !MyConfig.Verificacion)
            {
                List<MandarNuevo> eliminar = new List<MandarNuevo>();
                for (int i = 0; i < coleccion.Count; i++)
                {
                    if (coleccion[i].Value == null)
                    eliminar.Add(new MandarNuevo
                    {
                        Laboratorio = lab,
                        Dia = dgvHorarios.SelectedCells[i].ColumnIndex,
                        Hora = dgvHorarios.SelectedCells[i].RowIndex,
                    });
                }
                Eliminar(eliminar, lab);
            }
            List<MandarNuevo> Mandar = new List<MandarNuevo>();
            for (int i = 0; i < coleccion.Count; i++)
            {
                Mandar.Add(new MandarNuevo
                {
                    Laboratorio = lab,
                    Dia = dgvHorarios.SelectedCells[i].ColumnIndex,
                    Hora = dgvHorarios.SelectedCells[i].RowIndex,
                });
            }
            AbrirNuevaVentana(Mandar);
        }
        private void Eliminar(List<MandarNuevo> eliminar, int lab)
        {
            int quePaso = 0;
            for (int i = 0; i < eliminar.Count; i++)
            {
                bool result = ControlAlmacen.SetPrestamoLaboratorio(new PrestamoLaboratorio()
                {
                    Aula = lab,
                    Dia = eliminar[i].Dia,
                    Entrada = Tiempo(eliminar[i].Hora),
                }, 4);
                if (!result)
                    quePaso++;
            }
            if(quePaso == 0)
            {
                if (MyConfig.Confirmacion)
                {
                    frmMensaje M = new frmMensaje("Correcto", "Se dejaron libres todos los espacios para el laboratorio");
                    M.ShowDialog();
                }
            }
            else
            {
                if (MyConfig.Error)
                {
                    frmMensaje M = new frmMensaje("Error", "Se produjo un error al momento de dejar libre el espacio");
                    M.ShowDialog();
                }
            }

        }
        private string Tiempo(int pos)
        {
            switch (pos)
            {
                default:
                    return "07:00";
                    break;
                case 0:
                    return "07:00";
                    break;
                case 1:
                    return "08:00";
                    break;
                case 2:
                    return "09:00";
                    break;
                case 3:
                    return "10:00";
                    break;
                case 4:
                    return "11:00";
                    break;
                case 5:
                    return "12:00";
                    break;
                case 6:
                    return "13:00";
                    break;
                case 7:
                    return "14:00";
                    break;
                case 8:
                    return "15:00";
                    break;
                case 9:
                    return "16:00";
                    break;
                case 10:
                    return "17:00";
                    break;
                case 11:
                    return "18:00";
                    break;
                case 12:
                    return "19:00";
                    break;
                case 13:
                    return "20:00";
                    break;
                case 14:
                    return "21:00";
                    break;
                case 15:
                    return "22:00";
                    break;
            }
        }

    }
}
