﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmNuevoLab : Form
    {
        private List<MandarNuevo> Espacios;
        private cAlmacen ControlAlmacen;
        private Configuraciones MyConfig;
        private List<Persona> P;
        public frmNuevoLab(List<MandarNuevo> Recivir,Configuraciones Config)
        {
            InitializeComponent();
            ControlAlmacen = new cAlmacen();
            this.Espacios = Recivir;
            this.MyConfig = Config;
        }

        private void frmNuevoLab_Load(object sender, EventArgs e)
        {
            P = ControlAlmacen.GetPersona(new Persona(), 3);
            List<string> resps = new List<string>();
            for (int i = 0; i < P.Count; i++)
            {
                resps.Add(P[i].Nombre + " " + P[i].Paterno + " " + P[i].Materno);
            }
            cmbMateria.DataSource = resps;
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string resp = "";
            for (int i = 0; i < P.Count; i++)
            {
                if (P[i].Nombre + " " + P[i].Paterno + " " + P[i].Materno == cmbMateria.Text)
                    resp = P[i].ID;
            }
            int a = 0;
            for (int i = 0; i < Espacios.Count; i++)
            {
                bool res = ControlAlmacen.SetPrestamoLaboratorio(new PrestamoLaboratorio()
                {
                    Aula = Espacios[i].Laboratorio,
                    Dia = Espacios[i].Dia,
                    Entrada = Tiempo(Espacios[i].Hora),
                    Salida = Tiempo(Espacios[i].Hora + 1),
                    Responsable = resp,
                }, 1);
                if (!res)
                    a++;
            }
            if (a == 0)
            {
                if (MyConfig.Confirmacion)
                {
                    frmMensaje M = new frmMensaje("Correcto", "Se hiso el apartado correctamente");
                    M.ShowDialog();
                }
            }
            else
            {
                if (MyConfig.Error)
                {
                    frmMensaje M = new frmMensaje("Error", "No se pudo apartar el laboratorio");
                    M.ShowDialog();
                }
            }
            this.Close();
        }
        private string Tiempo(int pos)
        {
            switch (pos)
            {
                default:
                    return "07:00";
                    break;
                case 0:
                    return "07:00";
                    break;
                case 1:
                    return "08:00";
                    break;
                case 2:
                    return "09:00";
                    break;
                case 3:
                    return "10:00";
                    break;
                case 4:
                    return "11:00";
                    break;
                case 5:
                    return "12:00";
                    break;
                case 6:
                    return "13:00";
                    break;
                case 7:
                    return "14:00";
                    break;
                case 8:
                    return "15:00";
                    break;
                case 9:
                    return "16:00";
                    break;
                case 10:
                    return "17:00";
                    break;
                case 11:
                    return "18:00";
                    break;
                case 12:
                    return "19:00";
                    break;
                case 13:
                    return "20:00";
                    break;
                case 14:
                    return "21:00";
                    break;
                case 15:
                    return "22:00";
                    break;
            }
        }
    }
}
